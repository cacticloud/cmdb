package main

import (
	"github.com/cacticloud/cmdb/cmd"
)

func main() {
	cmd.Execute()
}
