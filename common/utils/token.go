package utils

import (
	"math/rand"
	"strings"
	"time"
)

func MakeBearer(length int) string {
	charlist := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	t := make([]string, length)
	r := rand.New(rand.NewSource(time.Now().UnixNano() + int64(length) + rand.Int63n(10000)))
	for i := 0; i < length; i++ {
		rn := r.Intn(len(charlist))
		w := charlist[rn : rn+1]
		t[i] = w
	}

	token := strings.Join(t, "")
	return token
}
