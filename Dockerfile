FROM registry.cn-hangzhou.aliyuncs.com/godev/golang:1.20 AS builder

LABEL stage=gobuilder

COPY . /src

ENV CGO_ENABLED 0
ENV GOOS linux
ENV GOARCH amd64
ENV GOPROXY https://goproxy.cn,direct
ENV GOPRIVATE github.com
ENV GO111MODULE on 
# ENV GOPRIVATE="*.gitlab.com"

WORKDIR /src
RUN echo "https://iginkgo18%40gmail.com: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@github.com" >> ~/.git-credentials && git config --global credential.helper store
RUN make build

FROM alpine

WORKDIR /app
EXPOSE 9001

COPY --from=builder /src/dist/cmdb /app/cmdb-api
COPY --from=builder /src/etc /app/etc

CMD ["./cmdb-api", "start", "-t", "env"]
