package apps

import (
	// 监控检查实现
	_ "github.com/cacticloud/cmdb/apps/health/impl"

	// 注册所有GRPC服务模块, 暴露给框架GRPC服务器加载, 注意 导入有先后顺序
	_ "github.com/cacticloud/cmdb/apps/book/impl"
)
