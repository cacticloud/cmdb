package book

import (
	"fmt"
	"github.com/cacticloud/cactikit/http/request"
	pb_request "github.com/cacticloud/cactikit/pb/request"
	"github.com/go-playground/validator"
	"github.com/rs/xid"
	"net/http"
	"time"
)

const (
	AppName = "book"
)

var (
	validate = validator.New()
)

func NewCreateBookRequest() *CreateBookRequest {
	return &CreateBookRequest{}
}

func (req *CreateBookRequest) Validate() error {
	return validate.Struct(req)
}

func (req *Book) Validate() error {
	return validate.Struct(req)
}

func (req *DescribeBookRequest) Validate() error {
	if req.Id == "" {
		return fmt.Errorf("book id required")
	}

	return nil
}

func (req *QueryBookRequest) Validate() error {
	return validate.Struct(req)
}

func NewBook(req *CreateBookRequest) (*Book, error) {
	if err := req.Validate(); err != nil {
		return nil, err
	}

	return &Book{
		Id:       xid.New().String(),
		CreateAt: time.Now().UnixMicro(),
		Name:     req.Name,
		Author:   req.Author,
	}, nil
}

func NewBookSet() *BookSet {
	return &BookSet{
		Items: []*Book{},
	}
}

func (s *BookSet) Add(item *Book) {
	s.Items = append(s.Items, item)
}

func NewDefaultBook() *Book {
	return &Book{}
}

func (i *Book) Update(req *UpdateBookRequest) error {
	i.UpdateAt = time.Now().UnixMicro()
	i.UpdateBy = req.UpdateBy
	i.Name = req.Data.Name
	i.Author = req.Data.Author
	return nil

}

func (i *Book) Patch(req *UpdateBookRequest) error {
	i.UpdateAt = time.Now().UnixMicro()
	i.UpdateBy = req.UpdateBy
	i.Name = req.Data.Name
	i.Author = req.Data.Author
	return nil
}

func NewDescribeBookRequest(id string) *DescribeBookRequest {
	return &DescribeBookRequest{
		Id: id,
	}
}

func NewQueryBookRequest() *QueryBookRequest {
	return &QueryBookRequest{
		Page: request.NewPageRequest(20, 1),
	}
}

func NewQueryBookRequestFromHTTP(r *http.Request) *QueryBookRequest {
	qs := r.URL.Query()

	return &QueryBookRequest{
		Page:     request.NewPageRequestFromHTTP(r),
		Keywords: qs.Get("keywords"),
	}
}

func NewPutBookRequest(id string) *UpdateBookRequest {
	return &UpdateBookRequest{
		Id:         id,
		UpdateMode: pb_request.UpdateMode_PUT,
		UpdateAt:   time.Now().UnixMicro(),
		Data:       NewCreateBookRequest(),
	}
}

func NewPatchBookRequest(id string) *UpdateBookRequest {
	return &UpdateBookRequest{
		Id:         id,
		UpdateMode: pb_request.UpdateMode_PATCH,
		UpdateAt:   time.Now().UnixMicro(),
		Data:       NewCreateBookRequest(),
	}
}

func NewDeleteBookRequestWithID(id string) *DeleteBookRequest {
	return &DeleteBookRequest{
		Id: id,
	}
}
