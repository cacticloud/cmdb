package impl

import (
	"context"
	"github.com/cacticloud/cactikit/app"
	"github.com/cacticloud/cmdb/apps/book"
	"github.com/cacticloud/cmdb/conf"
	"testing"
)

var (
	ins book.ServiceServer
)

//func TestQueryBook(t *testing.T) {
//	res, err := ins.QueryBook(context.Background(), book.NewQueryBookRequest())
//	if err != nil {
//		t.Fatal(err)
//	}
//	t.Log(res)
//}

func TestDescribeBook(t *testing.T) {
	res, err := ins.DescribeBook(context.Background(), book.NewDescribeBookRequest("cha78s1ocugl1ksse2e0"))
	if err != nil {
		t.Fatal(err)
	}
	t.Log(res)
}

//func TestCreateBook(t *testing.T) {
//	req := book.NewCreateBookRequest()
//	req.CreateBy = "ginkgo"
//	req.Name = "三体"
//	req.Author = "刘慈溪"
//	ss, err := ins.CreateBook(context.Background(), req)
//	if err != nil {
//		t.Fatal(err)
//	}
//	t.Log(ss)
//}

func init() {
	if err := conf.LoadConfigFromEnv(); err != nil {
		panic(err)
	}

	if err := app.InitAllApp(); err != nil {
		panic(err)
	}

	ins = app.GetGrpcApp(book.AppName).(book.ServiceServer)
}
