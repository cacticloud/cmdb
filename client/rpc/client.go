package rpc

import (
	"context"
	"fmt"

	"github.com/cacticloud/mcenter/client/rpc"
	"github.com/cacticloud/mcenter/client/rpc/resolver"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"github.com/cacticloud/cactikit/logger"
	"github.com/cacticloud/cactikit/logger/zap"
)

func NewClientSetFromEnv() (*ClientSet, error) {
	// 从环境变量中获取mcenter配置
	mc, err := rpc.NewConfigFromEnv()
	if err != nil {
		return nil, err
	}

	return NewClientSetFromConfig(mc)
}

// NewClientSetFromConfig NewClient todo
func NewClientSetFromConfig(conf *rpc.Config) (*ClientSet, error) {
	log := zap.L().Named("sdk.cmdb")
	// 加载mcenter client, cmdb基于mcenter client实现服务发现
	if !rpc.HasLoaded() {
		err := rpc.LoadClientFromConfig(conf)
		if err != nil {
			return nil, err
		}
	} else {
		log.Warnf("mecenter client is loaded, skip loaded agine")
	}

	ctx, cancel := context.WithTimeout(context.Background(), conf.Timeout())
	defer cancel()

	// 连接到服务
	conn, err := grpc.DialContext(
		ctx,
		fmt.Sprintf("%s://%s", resolver.Scheme, "cmdb"),
		grpc.WithPerRPCCredentials(rpc.NewAuthentication(conf.ClientID, conf.ClientSecret)),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithDefaultServiceConfig(`{"loadBalancingPolicy":"round_robin"}`),
		grpc.WithBlock(),
	)

	if err != nil {
		return nil, err
	}

	return &ClientSet{
		conf: conf,
		conn: conn,
		log:  log,
	}, nil
}

// Client 客户端
type ClientSet struct {
	conf *rpc.Config
	conn *grpc.ClientConn
	log  logger.Logger
}
